# スイッチの設定

例では**Cisco IOS + Catalyst 2940**を使用している。

* パスワード
* IPアドレスとゲートウェイ
* Portfast

一般的な初期設定はここら辺を参考に...

* http://beginners-network.com/catalyst_config_first.html

## configure

```
maas-test#sh run
Building configuration...

Current configuration : 1632 bytes
!
version 12.1
no service pad
service timestamps debug uptime
service timestamps log uptime
service password-encryption
!
hostname maas-test
!
enable secret 5 XXX
!
clock timezone JST 9
ip subnet-zero
!
!
spanning-tree mode pvst
no spanning-tree optimize bpdu transmission
spanning-tree extend system-id
no spanning-tree vlan 101
!
!
interface FastEthernet0/1
 switchport access vlan 101
 switchport mode access
 spanning-tree portfast trunk
!
interface FastEthernet0/2
 switchport access vlan 101
 switchport mode access
 spanning-tree portfast trunk
!
interface FastEthernet0/3
 switchport access vlan 101
 switchport mode access
 spanning-tree portfast trunk
!
interface FastEthernet0/4
 switchport access vlan 101
 switchport mode access
 spanning-tree portfast trunk
 !
interface FastEthernet0/5
 switchport access vlan 101
 switchport mode access
 spanning-tree portfast trunk
!
interface FastEthernet0/6
 switchport access vlan 101
 switchport mode access
 spanning-tree portfast trunk
!
interface FastEthernet0/7
 switchport access vlan 101
 switchport mode access
 spanning-tree portfast trunk
!
interface FastEthernet0/8
 switchport access vlan 101
 switchport mode access
 spanning-tree portfast trunk
!
interface GigabitEthernet0/1
 description Uplink
 switchport access vlan 101
 switchport mode access
!
!
interface Vlan1
 no ip address
 no ip route-cache
 shutdown 
!
interface Vlan101  ←VLAN設定はその環境に合わせます。
 ip address 172.16.xxx.xxx 255.255.0.0
 no ip route-cache
!
ip default-gateway 172.16.xxx.xxx
ip http server
!
line con 0
 password 7 XXX
 logging synchronous
 login
line vty 0 4
 password 7 XXX
 login
line vty 5 15
 login
!         
!
end
```


## vlan configure

```
maas-test#sh vlan  ←ポートとVLANの設定

VLAN Name            Status    Ports
---- --------------- --------- -------------------------------
1    default         active    
101  vlan101         active    Fa0/1, Fa0/2, Fa0/3, Fa0/4
                               Fa0/5, Fa0/6, Fa0/7, Fa0/8
                               Gi0/1

maas-test#write  ←設定を保存
```