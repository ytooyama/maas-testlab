# MAASのセットアップ

ネットワークの設定が終わったら、次にMAASをセットアップします。

## 推奨するマシンのスペック

* Dual Core以上
* メモリー4GB以上
* SSDストレージを推奨
* 1Gb Ethernet

## インストールに利用したマシン

* HP Compaq 6710b
  * メモリー4GB
  * SSD 256GB
  * 1Gb Ethernet

## Ubuntu Serverのインストール

マシンにUbuntu Serverをインストールします。
このマシンでMAASを動作させます。


## インターフェイスの設定

物理側はDHCPサーバーから割り当てられたIPアドレスを使います。
MAAS内部でDNSとDHCPを動かす必要があり、既存のネットワークへ影響を与えないようにするために、
MAASへの提供用はIPエイリアスによって設定した側のネットワークを利用します。

MAASで管理するノードはこちら側のネットワークで管理します。

### IPアドレスの設定

```
maas:~$ vi /etc/network/interfaces
...
# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto ens1
iface ens1 inet dhcp  <-物理ネットワーク側(DHCPなり、Staticなりを設定)

auto ens1:1   <-MAASで使う側のネットワーク。適当なものを
iface ens1:1 inet static
address 192.168.0.1
netmask 255.255.255.0
```

### IPフォワードを設定

```
maas:~$ sudo vi /etc/sysctl.conf
net.ipv4.ip_forward=1

maas:~$ sysctl -p /etc/sysctl.conf 
```

## 再起動

システムを再起動して、IPアドレスをシステムに設定します。

```
maas:~$ sudo reboot
```

## MAASのインストール

MAASをaptコマンドでインストールします。

```
maas:~$ sudo apt update -q
maas:~$ sudo apt install maas
```

## MAASの管理ユーザーの作成

MAASの管理用のユーザーを作成するため、次のコマンドを実行します。

```
maas:~$ sudo maas createadmin
```

## MAASの初期設定

次のような流れで、 MAASの初期設定を行います。

* MAAS Dashboardにログイン
* 初期設定をおこなう
  * SSHキーペア
  * MAASイメージ

* サブネット設定を開く
* サブネットを追加
  * [手順を参考に設定](https://docs.ubuntu.com/maas/2.2/en/installconfig-networking)

|  項目   |      設定      |
| ------- | -------------- |
| CIDR    | 192.168.0.0/24 |
| Gateway | 192.168.0.1    |
| DNS     | 192.168.0.1    |


* 追加後のイメージ

一つ目のサブネットはそれぞれの環境により、表示されるIPアドレス範囲が異なります。
二つ目のmaas-netと名前をつけたサブネット側でMAASの管理ネットワークを運用します。

![OpenStack Dashbpard View](./img/maas-subnet.png)

## MAAS Region ControllerのIPアドレスの変更

デフォルトでは一つ目のNIC側のIPアドレスになっているので、これをMAASで作成したネットワーク側に変更する。切り替え後は再起動する。

```
maas:~$ sudo dpkg-reconfigure maas-region-controller
192.168.0.1に変更

maas:~$ sudo reboot
```

* サブネットでDHCPサーバーを動かす
  * [手順を参考に設定](https://docs.ubuntu.com/maas/2.2/en/installconfig-network-dhcp)

|項目      |始め         |終わり       |
|----------|-------------|-------------|
|MAAS CIDR |192.168.0.0/24 |-          | 
|MAAS DNS  |192.168.0.1  |-            |
|Reseved   |192.168.0.1  |192.168.0.30 |
|Dynamic   |192.168.0.50 |192.168.0.254|


* IPマスカレードの設定をiptablesに設定

```
$ sudo iptables -t nat -A POSTROUTING -o ens1 -j MASQUERADE
```

## この後の作業

サーバーの電源をオンにして、[Enlist](https://docs.ubuntu.com/maas/2.2/en/nodes-add) , [コミッショニング](https://docs.ubuntu.com/maas/2.2/en/nodes-commission) , [デプロイ](https://docs.ubuntu.com/maas/2.2/en/nodes-deploy) のように実行します。

状況はリモートコンソールで確認します。

サーバーによってはEnlist後に[Power-typeを手動で設定](https://docs.ubuntu.com/maas/2.2/en/nodes-power-types) する必要があることがあります。